import glob
import joblib
import random
import jsonlines
import unicodedata
from joblib import Parallel, delayed
from copy import deepcopy
from collections import defaultdict
from threading import Thread, Condition
from sacremoses import MosesTokenizer

from UniversalUtils import *
from Constants import *


# Get CPU count
n_cpu = os.cpu_count()


def rem_punct(s):
    s = s.strip()
    if s[-1] == '.': s = s[:-1]
    incl = [c not in punct for c in s]
    if len(s) > 0:
        for i in range(len(s)):
            if not incl[i] and (i == 0 or s[i - 1].isnumeric()) and (i == (len(s) - 1) or s[i + 1].isnumeric()):
                incl[i] = True
                if s[i] in "-‒―—": s = s[:i] + ' ' + s[i + 1:]
    return ''.join([s[i] for i in range(len(s)) if incl[i]]).strip()

# Load claims ('test_10' means the first 10 verifiable claims from the training subset, used for Q2-3)
def load_claims(n=10, offs=0, test_10=True, dev=False, test=False, verifiable_only=True):
    claims = []
    claims_vocab = set()
    i_ = 0
    with open(data_dir + ("shared_task_dev" if dev else ("shared_task_test" if test else "train")) + ".jsonl", 'r') as f:
        for a_ in jsonlines.Reader(f):
            if dev or test or ((a_["id"] in test_claim_ids) ^ (not test_10)):
                if verifiable_only and "verifiable" in a_ and a_["verifiable"] != "VERIFIABLE": continue
                i_ += 1
                if i_ <= offs: continue
                if "verifiable" in a_ and a_["verifiable"] == "VERIFIABLE" and "evidence" in a_:
                    for j in range(len(a_["evidence"])):
                        for k in range(len(a_["evidence"][j])):
                            a_["evidence"][j][k][2] = unicodedata.normalize('NFC', a_["evidence"][j][k][2])
                claims.append(a_)
                claims[-1]["vocab"] = defaultdict(int)
                for w in rem_punct(a_["claim"].replace('-LRB-', '').replace('-RRB-', '').replace("'s", '')).split():
                    claims[-1]["vocab"][w] += 1
                    claims_vocab.add(w)
            if i_ == n + offs: break
    return claims, claims_vocab

# Load wikipedia documents for Top-K retrieval
max_number_len = 5            # Ignore words which are simply numbers of length greater than this
max_alphanum_len = 8          # Ignore alphanumeric words of length greater than this
def load_line(l):
    return [ rem_punct(w) for w in l.split()[1:] if w != \
             '' and not ((any(c.isnumeric() for c in w) and len(w) > max_alphanum_len) or \
             (w.isnumeric() and len(w) > max_number_len)) ]
def load_docs(fs, vocab, load_tf=False, n_jobs=n_cpu):
    i_, wiki, n_t = 0, {}, {}
    if load_tf: tfs = {}
    for fn in fs:
        ids_ = []
        all_lines = []
        with open(fn, 'r') as f:
            for a_ in jsonlines.Reader(f):
                if a_["id"] == "" or a_["lines"] == "": continue
                ids_.append(a_["id"])
                all_lines.append((a_["id"],
                    rem_punct(a_["id"].replace('_', ' ').replace('-LRB-', '').replace('-RRB-', '').
                        replace("'s", '') + ' '.join([l for l in a_[
                        "lines"].replace('-LRB-', '').replace('-RRB-', '').replace("'s", '').split('\n') if \
                         len(l.split(' ')) > 0 and not (len(l.split(' ')) == 1 and l.strip().isnumeric())]))))
        print(len(all_lines), ",", str(i_) + " / " + str(len(fs)))
        all_lines_ = joblib.Parallel(n_jobs=n_jobs, verbose=5)(delayed(load_line)(l) for _, l in all_lines)
        for i in range(len(ids_)):
            id_, l = ids_[i], all_lines_[i]
            doc_tf, total = {}, 0
            for w in l:
                if load_tf:
                    if w in tfs: tfs[w] += 1
                    else:        tfs[w] = 1
                if w in vocab:
                    if w in doc_tf: doc_tf[w] += 1
                    else:           doc_tf[w] = 1
                total += 1
            if total == 0: continue
            for w in set(l):
                if w in n_t: n_t[w] += 1
                else:        n_t[w] = 1
            wiki[id_] = doc_tf, total
        i_ += 1
    if load_tf: return tfs, wiki, n_t
    return wiki, n_t

def load_docs_lines():       # Load wikipedia documents and preprocess for ELMo
    fs = glob.glob(data_dir + wiki_dir + "*.jsonl")
    elmo_tokenizer = MosesTokenizer()  # Tokenise in the same way as ELMo's original training
    transl_elmo = lambda s: ' '.join(elmo_tokenizer.tokenize(s, escape=False)).replace(" ' s ", " 's ").replace("-LSB-", "["). \
        replace("-RSB-", "]")
    i_ = 0
    wiki_ls = {}
    for fn in fs:
        with open(fn, 'r') as f:
            for a_ in jsonlines.Reader(f):
                if a_["id"] == "" or a_["lines"] == "":
                    continue
                ls = a_["lines"].replace('-LRB-', '(').replace('-RRB-', ')').split('\n')
                lines = {}
                cur_num = 0
                for l in ls:
                    if '\t' not in l:
                        if l.isnumeric():
                            continue
                        line_num = cur_num
                    else:
                        first_tab = l.index('\t')
                        line_num, l = l[:first_tab], l[first_tab + 1:]
                        if not line_num.isnumeric():
                            line_num = str(cur_num)
                    if '\t' in l:
                        l = l[:l.index('\t')]
                    if len(l.split(' ')) > 0 and not (len(l.split(' ')) == 1 and l.strip().isnumeric()):
                        l = [w for w in transl_elmo(l).split() if w != \
                             '' and not ((any(c.isnumeric() for c in w) and len(w) > max_alphanum_len) or \
                             (w.isnumeric() and len(w) > max_number_len))]
                        if len(l) > 0:
                            cur_num = int(line_num)
                            lines[cur_num] = ' '.join(l)
                    cur_num += 1
                wiki_ls[a_['id']] = lines
        i_ += 1
        sys_print("\r" + str(i_) + " / " + str(len(fs)))
    return wiki_ls


def save_ld(data, name, pad=True, compress=False):
    if pad:
        create_folder(data_dir + learning_data_dir)
        name = data_dir + learning_data_dir + name + ".data"
    joblib.dump(data, name, compress=compress)

def load_ld(name, pad=True):
    if pad: name = os.path.join(data_dir, learning_data_dir, name + ".data")
    return joblib.load(name)


def save_json(obj, name, pad=True):
    if pad:
        name = os.path.join(data_dir, name + '.json')
    with open(name, 'w') as outfile:
        json.dump(obj, outfile)

def load_json(name, pad=True):
    if pad:
        name = os.path.join(data_dir, name + '.json')
    try:
        with open(name, 'r') as infile:
            return json.load(infile)
    except:
        print("Error loading json file [" + name + "]:")
        print(sys.exc_info())
        print(sys.exc_info()[1])
        print(sys.exc_info()[2])
        return None



#
#  db_add
#
#  Adds (concatenates) a dataframe to a csv database & returns updated database
#
#  df               = new data to add
#  path             = path of database
#  return_region    = if not None, region for which to return data for
#  **kwargs         = keyword args for database-loading pd.read_csv call
#
def db_add(df, path, **kwargs):
    new_df = None
    if os.path.exists(path):
        with open(path, 'a') as db:
            df.to_csv(db, header=False)
        new_df = pd.read_csv(path, **kwargs)
    else:
        df.to_csv(path)
        new_df = df
    return new_df

#
#  db_upd
#
#  Updates (merges) a dataframe into a csv database & returns updated database
#  Only difference to db_add is that df can contain new data for existing keys
#
def db_upd(df, path, **kwargs):
    new_df = None
    if os.path.exists(path):
        new_df = pd.read_csv(path, **kwargs)
        for key in list(df.index):
            new_df.loc[key] = list(df.loc[key])
        new_df.to_csv(path)
    else:
        df.to_csv(path)
        new_df = df
    return new_df


