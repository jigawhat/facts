from Utils import *


def batch_sims(docs, sim_func, **ps):
    return [sim_func(*doc, **ps) for doc in docs]

# Define cross validation partitioning
def gen_cv_inds(k, n, randomise=True, shuffle=False):
    all_inds = list(range(n))
    if randomise: np.random.shuffle(all_inds)
    train_n = int(np.floor(n * (1.0 - (1.0 / k))))
    test_n = n - train_n
    test_inds  = [all_inds[i * test_n: \
                    ((i + 1) * test_n) if i < (k - 1) else None] for i in range(k)]
    train_inds = [[i for i in range(n) if i not in inds] for inds in test_inds]
    if shuffle:
        for i in range(k):
            np.random.shuffle(test_inds[i]), np.random.shuffle(train_inds[i])
    return train_inds, test_inds

def harmonica(a, b):
    return 2.0 / ((1.0 / np.maximum(a, 1e-10)) + (1.0 / np.maximum(b, 1e-10)))
def weighted_harmonica(a, b, beta=1.0):
    return (1 + (beta ** 2)) * a * b / np.maximum(((beta ** 2) * a) + b, 1e-10)

# Define global scoring functions
def accuracy(y, y_hat):
    return np.sum(y == y_hat) / len(y)
def precision(y, y_hat):
    return np.sum(np.logical_and(y, y_hat)) / np.sum(y_hat)
def recall(y, y_hat):
    return np.sum(np.logical_and(y, y_hat)) / np.sum(y)
def f_beta(y, y_hat, beta):
    p, r = precision(y, y_hat), recall(y, y_hat)
    return weighted_harmonica(p, r, beta=beta)
def f1_score(y, y_hat):
    return harmonica(precision(y, y_hat), recall(y, y_hat))
def f2_score(y, y_hat):
    return f_beta(y, y_hat, 2.0)
def f3_score(y, y_hat):
    return f_beta(y, y_hat, 3)
def f10_score(y, y_hat):
    return f_beta(y, y_hat, 10)
def negative_log_loss(y, y_hat):
    return -( -np.sum((y * np.log(np.maximum(y_hat, 1e-10))) + \
                ((1 - y) * np.log(np.maximum(1 - y_hat, 1e-10)))) )

# Define retrieval performance metric
def eval_acc(top_docs, claims, n=5, verbose=True, extra_fs=[]):
    if verbose: print(" Scoring evidence retrieval...")
    # macro = evidence-documents Fever retrieval score
    # micro = claim averaged
    # nano = evidence-set averaged
    # pico = evidence-document averaged
    nano_count = 0   # Number of evidence sets
    macro_r, macro_p, macro_f1, micro_r, nano_r, pico_r = [[] for _ in range(6)]
    for i in range(len(claims)):
        r = top_docs[i][-n:]
        docs = set()
        eva_set_macro_r = []
        for eva_set in claims[i]["evidence"]:
            d = set([eva[2] for eva in eva_set])
            docs |= d
            included = [d_ in r for d_ in d]
            pico_r += included
            eva_set_recall = np.mean(included)
            nano_r.append(eva_set_recall)
            nano_count += n
            eva_set_macro_r.append(all(included))
        docs = list(docs)
        docs_incl = [doc in r for doc in docs]
        micro_r.append(np.mean(eva_set_macro_r))
        macro_r.append(any(eva_set_macro_r))
        macro_p.append(np.mean([doc in docs for doc in r]))
    macro_r, macro_p = np.mean(macro_r), np.mean(macro_p)
    macro_f1 = harmonica(macro_r, macro_p)
    extra_f_scores = []
    micro_p = sum(micro_r) / (n * len(claims))
    micro_r = np.mean(micro_r)
    micro_f1 = harmonica(micro_r, micro_p)
    nano_p = sum(nano_r) / nano_count
    nano_r = np.mean(nano_r)
    nano_f1 = harmonica(nano_r, nano_p)
    pico_p = sum(pico_r) / (n * len(claims))
    pico_r = np.mean(pico_r)
    pico_f1 = harmonica(pico_r, pico_p)
    if verbose: print(macro_r, micro_r, nano_r, pico_r, \
        macro_p, micro_p, nano_p, pico_p, macro_f1, micro_f1, nano_f1, pico_f1)
    return macro_r, micro_r, nano_r, pico_r, \
        macro_p, micro_p, nano_p, pico_p, macro_f1, micro_f1, nano_f1, pico_f1

# Define relevance classification performance metric
def eval_rel(probs, ci, ei, cs, n=5, strict_rel=True, extra_fs=[], print_preds=False, verbose=False):
    # macro = evidence-documents Fever retrieval score
    # micro = claim averaged
    # nano = evidence-set averaged
    # pico = evidence-document averaged
    nano_count = 0   # Number of evidence sets
    macro_r, macro_p, macro_f1, micro_r, nano_r, pico_r = [[] for _ in range(6)]
    hit_count, occ_count = 0, 0
    claims_p = set(ci)
    for c_i in range(len(cs)):
        claim = cs[c_i]
        claim_i = claim["claim_i"]
        if claim_i not in claims_p: continue
        inds = np.nonzero(ci == claim_i)[0]
        prob_ord = np.argsort(probs[inds.tolist()])[::-1]
        top = inds[prob_ord[:n]].tolist()
        if strict_rel: top = [i for i in top if probs[i] >= 0.5]
#             new_top = [i for i in top if probs[i] >= 0.5]
#             if len(new_top) > 0: top = new_top
#             else               : top = top[:1]
        r = set([(v[0], int(v[1])) for v in ei[top]]) if len(top) > 0 else set()
        r_list = list(r)
        hit_count += len(r)
        evidence = set()
        eva_set_macro_r = []
        for eva_set in claim["evidence"]:
            e = set([tuple(eva[2:]) for eva in eva_set])
            evidence |= e
            included = [e_ in r for e_ in e]
            occ_count += len(r)
            pico_r += included
            eva_set_recall = np.mean(included)
            nano_r.append(eva_set_recall)
            nano_count += len(r)
            eva_set_macro_r.append(all(included))
        if print_preds: pr((claim["claim"], [(r[0], r[1], r in evidence) for r in r_list]))
        evidence = list(evidence)
        evidence_incl = [e in r for e in evidence]
        micro_r.append(np.mean(evidence_incl))
        macro_r.append(any(eva_set_macro_r))
        macro_p.append(np.mean([e in evidence for e in r]) if len(r) > 0 else 1.0)
    macro_r, macro_p = np.mean(macro_r), np.mean(macro_p)
    macro_f1 = harmonica(macro_r, macro_p)
    extra_f_scores = []
    micro_p = sum(micro_r) / hit_count
    micro_r = np.mean(micro_r)
    micro_f1 = harmonica(micro_r, micro_p)
    nano_p = sum(nano_r) / nano_count
    nano_r = np.mean(nano_r)
    nano_f1 = harmonica(nano_r, nano_p)
    pico_p = sum(pico_r) / occ_count
    pico_r = np.mean(pico_r)
    pico_f1 = harmonica(pico_r, pico_p)
    if verbose: print(macro_f1, micro_f1, nano_f1, pico_f1, \
        macro_r, micro_r, nano_r, pico_r, macro_p, micro_p, nano_p, pico_p)
    return macro_f1, macro_r, macro_p, \
           micro_f1, micro_r, micro_p, \
           nano_f1,  nano_r,  nano_p,  \
           pico_f1,  pico_r,  pico_p


