import string


data_dir = "data/"
wiki_dir = "wiki-pages/"
learning_data_dir = "learning_data/"
store_folder = "H:/Code/IR/data/learning_data/"

Y_labels = ['SUPPORTS', 'REFUTES', 'NOT ENOUGH INFO']

punct = string.punctuation + "‒―—"
test_claim_ids = [75397, 150448, 214861, 156709, 129629, 33078, 6744, 226034, 40190, 76253]

