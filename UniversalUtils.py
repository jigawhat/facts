#
#  Universal Utilities v1.0
#

import os
import sys
import time
import json
import shutil
import pprint
from collections import OrderedDict
import pandas as pd
import numpy as np


def err_pr(o):
    # return
    pr(o)

def approx_eq(a, b, tol=0.00001):
    return abs(a - b) < tol

def powerset(s):
    r = [[]]
    for e in s:
        r += [x+[e] for x in r]
    return r

def invert_dict(d):
    return dict([[v, k] for k, v in d.items()])

def get_nearest(l, x):
    return min(l, key=lambda y: abs(x - y))
def get_nearest_i(l, x):
    return np.argmin(np.abs(np.array(l) - x))

def dummy_code(i, n):
    x = np.zeros(n)
    x[i] = 1
    return x

def a_E(d):
    return next(iter(d.values()))

def leng(obj):
    return len(obj) if (type(obj) == list or type(obj) == np.array) else 1

def hard_int(x):
    return 0 if not x else int(x)

def hard_float(x):
    return 0. if not x else float(x)

def rem_nones(l):
    return [0 if x is None else x for x in l]

def get_list_is_dict(l):
    return dict(zip(l, range(len(l))))

roman_numerals = {
    'I': 1,
    'II': 2,
    'III': 3,
    'IV': 4,
    'V': 5,
    'VI': 6,
    'VII': 7,
    'VIII': 8,
    'IX': 9,
    'X': 10,
}

roman_numerals_inv = invert_dict(roman_numerals)

def deromanize(n):
    return roman_numerals[n.upper()]

def romanize(n):
    return roman_numerals_inv[int(n)]

# Time methods
def one_month_ago(t=time.time()):
    return (t - (30.0 * 24.0 * 60.0 * 60.0)) * 1000
def one_week_ago(t=time.time()):
    return (t - (7.0 * 24.0 * 60.0 * 60.0)) * 1000
def one_day_ago(t=time.time()):
    return (t - (1.0 * 24.0 * 60.0 * 60.0)) * 1000
def one_hour_ago(t=time.time()):
    return (t - (1.0 * 60.0 * 60.0)) * 1000

# Flatten an ordered dictionary (-> one value per key)
def flat_od_keys(d):
    return sum([([k] if type(d[k]) not in [list, np.ndarray] else \
        [k + '_x' + str(i) for i in range(len(d[k]))]) for k in d], [])
def flat_od(d, keys=None):
    return OrderedDict(zip(flat_od_keys(d) if keys is None else keys,
                           np.hstack(d.values())))

    # ks = [k for k in d.keys() if type(d[k]) in [list, np.ndarray]]
    # for k in ks:
    #     v = d[k]
    #     for i in range(len(v)):
    #         d[k + '_x' + str(i)] = v[i]
    #     del d[k]
    # return d

# Add to a possibly empty (==None) pandas dataframe
def df_add(df, X, index, index_name):
    Y = X
    if not isinstance(X[0], OrderedDict):
        Y = [OrderedDict(x) for x in X]
    if df is None:
        df = pd.DataFrame(Y, index=index)
        df.index.name = index_name
    else:
        for i in range(len(index)):
            df.loc[index[i]] = list(Y[i].values())
    return df

# Print directly using standard output
def sys_print(obj):
    sys.stdout.write(str(obj))
    sys.stdout.flush()

# Print dictionaries (etc) in a human readable way
pp = pprint.PrettyPrinter(indent=4)
def pr(obj):
    pp.pprint(obj)

# Format an iterable of numbers for printing
def fm_nums(iterable, round_n):
    return [float("{:,}".format(round(n, round_n))) for n in iterable]

# Create folder, optionally overwriting existing folder
def create_folder(path, overwrite=False):
    if(os.path.isdir(path)):
        if(overwrite):
            shutil.rmtree(path)
        else:
            return
    os.mkdir(path)

# Extract file name from a path string
def deglob_fn(path, ext=None):
    return (path if ext is None else path.split(ext)[0]).split(
        '/')[-1].split('\\')[-1]

def conv_dlib(X):
    return np.vstack([np.hstack(list(x.values())) for x in X])


