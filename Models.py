import numpy as np
from scipy.stats import truncnorm
from sklearn.base import BaseEstimator, ClassifierMixin
from sklearn.pipeline import *
from sklearn.preprocessing import *
from sklearn.decomposition import *
from sklearn.feature_selection import *

from Retrieval import *


def sigmoid(x):
    return 1 / (1 + np.exp(-x))

def init_weights(typ, scale, size, a=-2.0, b=-2.0):
    if typ == 'uniform':
        return np.random.uniform(a * scale, b * scale, size)
    if typ == 'normal':
        return np.random.normal(0, scale, size)
    if typ == 'trunc_normal':
        return truncnorm.rvs(a, b, 0, scale, size)




# Binary classification gradient descent logistic regression implementation
class LR(BaseEstimator, ClassifierMixin):
    def __init__(self,
            eta=1e-2,               # Learning rate
            C=1.0,                  # Inverse regularisation strength
            regularisation="l2",    # Weights regularisation
            bias=True,              # Add bias term
            tol=1e-4,               # Tolerance before we consider a change in loss to be significant
            no_imp_stop=5,          # Stop criterion (no improvement after this many gradient descent steps)
            max_iter=10**7,         # Maximum number of iterations
            w_init='normal',        # Weights initialisation function (uniform, normal, or truncated normal)
            w_scale=1e-2,           # Weights initialisation scale
            w_a=-2.0,               # Weights initialisation range
            w_b=2.0,
            verbose=True,           # Print progress
            print_steps=10,         # Print progress every print_steps gradient descent steps
            return_steps=False,     # Store iteration index and loss every print_steps steps (in self.steps)
            score_funcs=[accuracy, precision, recall, f1_score, f2_score]):
        self.eta = eta
        self.C = C
        self.regularisation = regularisation
        self.bias = bias
        self.tol = tol
        self.no_imp_stop = no_imp_stop
        self.max_iter = max_iter
        self.w_init = w_init
        self.w_scale = w_scale
        self.w_a = w_a
        self.w_b = w_b
        self.score_funcs = score_funcs
        self.verbose = verbose
        self.print_steps = print_steps
        self.return_steps = return_steps

        self.steps = []
        self._classes = [0, 1]

    def fit(self, X, y):
        if self.bias:
            X = np.hstack([X, np.ones((X.shape[0], 1))])

        self.w = deepcopy(self.w_init)
        if isinstance(self.w, str):
            self.w = init_weights(self.w_init, self.w_scale, X.shape[1], a=self.w_a, b=self.w_b)

        itr = 0
        itr_no_imp = 0
        stop_crit = "max_iter"
        min_L = np.inf
        best_w = self.w
        best_itr = 0
        steps = []
        if self.verbose:
            print('|  iteration  |   loss   |  ' + '   '.join([fun.__name__ + '   |' for fun in self.score_funcs]))
        while True:
            y_hat = sigmoid(np.dot(X, self.w))  # Compute loss
            L = -np.sum((y * np.log(np.maximum(y_hat, 1e-15))) + ((1 - y) * np.log(np.maximum(1 - y_hat, 1e-15))))
            if self.regularisation == 'l1':
                L += np.sum(np.abs(self.w)) / self.C
            elif self.regularisation == 'l2':
                L += np.dot(self.w, self.w) * 0.5 / self.C

            if self.verbose and itr % self.print_steps == 0:
                outs = [itr, L] + [round(fun(y, np.round(y_hat)), 7) for fun in self.score_funcs]
                print('|  ' + '  '.join([str(out) + '  |' for out in outs]))
            if self.return_steps and itr % self.print_steps == 0: self.steps.append([itr, L])

            if itr == self.max_iter:  # Stopping criteria
                break
            if min_L - L > self.tol:
                itr_no_imp = 0
                min_L = L
                best_w = self.w
                best_itr = itr
            else:
                itr_no_imp += 1
            if itr_no_imp == self.no_imp_stop:
                stop_crit = "no_imp_stop"
                break

            errs = y_hat - y   # Compute loss gradient
            grad = np.dot(X.T, errs)
            if self.regularisation == 'l1':
                grad += np.sign(self.w) / self.C
            elif self.regularisation == 'l2':
                grad += self.w / self.C
            self.w -= self.eta * grad
            itr += 1

        self.w = best_w
        if self.verbose:
            print("Finished training (" + (("no loss improvement for " + str(itr_no_imp) + " steps)") \
                if stop_crit == "no_imp_stop" else "maximum number of iterations"))
            print("Final model (best loss weights) evaluation:")
            y_hat = sigmoid(np.dot(X, self.w))
            outs = [best_itr, min_L] + [round(fun(y, np.round(y_hat)), 7) for fun in self.score_funcs]
            print('|  iteration  |   loss   |  ' + '   '.join([fun.__name__ + '   |' for fun in self.score_funcs]))
            print('|  ' + '  '.join([str(out) + '  |' for out in outs]))
        return self

    def predict_proba(self, X):
        if self.bias:
            X = np.hstack([X, np.ones((X.shape[0], 1))])
        y_hat = np.atleast_2d(sigmoid(np.dot(X, self.w))).T
        return np.hstack([1 - y_hat, y_hat])

    def predict(self, X):
        return np.round(self.predict_proba(X))


